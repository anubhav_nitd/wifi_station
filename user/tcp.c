#include "tcp.h"
#include <ip_addr.h>
#include <espconn.h>
#include <os_type.h>
#include <c_types.h>
#include <user_interface.h>
#include <mem.h>
#include "tasks.h"
#include <osapi.h>

bool tcp_tx_in_progress;

static os_timer_t tx_slowdown_timer;
static bool       timer_is_running;

static void timer_func(void *arg);

void ICACHE_FLASH_ATTR incoming_tcpreq_cb(void *arg, char *pdata, unsigned short len) {
  unsigned short i;
  for(i = 0; i < len; i++) {
    if((i+5 < len)) {
      if(!os_strncmp(pdata+i, "GET /", 5)) {
        struct tcp_req_params *params = (struct tcp_req_params *) os_malloc(sizeof(struct tcp_req_params));
        params->client   = (struct espconn *) arg;
        params->prequest = (char *) (pdata+i);
        system_os_post(USER_TASK_PRIO_0, http_SERVE_GET_REQUESTS, (os_param_t) params);
        break;
      }
    }
  }
  //pdata[len-1] = '\0';
  //os_printf("%s", pdata);
}

void ICACHE_FLASH_ATTR tcpsent_cb(void *arg) {
  tcp_tx_in_progress = false;
}

esp_tcp tcpsettings;
struct espconn tcpconn;

void ICACHE_FLASH_ATTR tcp_set_listener() {
  tcpsettings.local_port = 80;

  tcpconn.type            = ESPCONN_TCP;
  tcpconn.proto.tcp       = &tcpsettings;
  tcpconn.recv_callback   = incoming_tcpreq_cb;
  tcpconn.sent_callback   = tcpsent_cb;
  
  espconn_accept(&tcpconn);
}

void ICACHE_FLASH_ATTR tcp_schedule_data_tx(struct tcp_txdata_node *list) {
  static uint16 max_data_len_low_current_consumption = 3000;
  if(tcp_tx_in_progress || timer_is_running) {
    system_os_post(USER_TASK_PRIO_0, tcp_SCHEDULE_DATA_TX, (os_param_t) list);
    return;
  }
  if(list->data == NULL) {
    espconn_disconnect(list->client);
  }
  else {
    if(list->data_len > max_data_len_low_current_consumption) {
      espconn_send(list->client, list->data, max_data_len_low_current_consumption);
      list->data     += max_data_len_low_current_consumption;
      list->data_len -= max_data_len_low_current_consumption;
      system_os_post(USER_TASK_PRIO_0, tcp_SCHEDULE_DATA_TX, (os_param_t) list);
      
      os_timer_disarm(&tx_slowdown_timer);
      os_timer_setfn(&tx_slowdown_timer, (os_timer_func_t *) timer_func, NULL);
      os_timer_arm(&tx_slowdown_timer, 10, 0);
      timer_is_running = true;
    }
    else {
      list->next->client = list->client;
      system_os_post(USER_TASK_PRIO_0, tcp_SCHEDULE_DATA_TX, (os_param_t) list->next);
      espconn_send(list->client, list->data, list->data_len);
      
      // The function calls above have had their arguments passed to them by value,
      // so it must be safe to delete the dynamically allocated node now.
      os_free(list);
    }
    tcp_tx_in_progress = true;
  }
}

static void timer_func(void *arg) {
  timer_is_running = false;
}
