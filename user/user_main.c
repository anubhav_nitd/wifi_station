#include <user_interface.h>
#include <os_type.h>
#include <osapi.h>
#include <mem.h>
#include "tasks.h"
#include "user_config.h"
#include "dvc.h"

char SSID_OF_AP_TO_CONNECT_TO[] = "NIT-WIFI";
struct ip_info user_conf_current_conn_ip;

/***********************
 * Function prototypes *
 ***********************/

void ICACHE_FLASH_ATTR init_done_cb();
void ICACHE_FLASH_ATTR wifi_event_cb(System_Event_t *event);

/*****************
 * Init function *
 *****************/
void ICACHE_FLASH_ATTR user_init() {
  uart_div_modify(0, UART_CLK_FREQ / 115200); // done to set os_printf baud to 115200
  system_os_task(task_priority_zero_handler, USER_TASK_PRIO_0, task_Q_P0, task_QLEN_P0);

  wifi_set_event_handler_cb(wifi_event_cb);
  wifi_set_opmode(STATION_MODE);
  system_init_done_cb(init_done_cb);
}

void ICACHE_FLASH_ATTR init_done_cb() {
  i2c_init();
  dvc_lcd_init();
  dvc_init_data_json();
  wifi_station_set_hostname("anubhav-esp8266");

  struct station_config ap_to_connect_to;
  os_strcpy(ap_to_connect_to.ssid, SSID_OF_AP_TO_CONNECT_TO);
  //os_strcpy(ap_to_connect_to.password, "");
  ap_to_connect_to.bssid_set = 0;

  if(wifi_station_set_config(&ap_to_connect_to)) wifi_station_connect();

}

void ICACHE_FLASH_ATTR wifi_event_cb(System_Event_t *event) {
  switch(event->event) {
    case EVENT_STAMODE_GOT_IP:
      user_conf_current_conn_ip.ip        = event->event_info.got_ip.ip;
      user_conf_current_conn_ip.netmask   = event->event_info.got_ip.mask;
      user_conf_current_conn_ip.gw        = event->event_info.got_ip.gw;
      
      // Send network name to LCD
      dvc_lcd_send_data(SSID_OF_AP_TO_CONNECT_TO, os_strlen(SSID_OF_AP_TO_CONNECT_TO));

      // Send command to LCD to go to next row
      dvc_lcd_gotoRC(1, 0);

      // Send IP address of device to LCD
      uint8 *busdata = (uint8 *) os_malloc(sizeof(uint8) * 16);
      os_sprintf(busdata, IPSTR, IP2STR(& ((event->event_info).got_ip.ip.addr)));
      dvc_lcd_send_data(busdata, os_strlen(busdata));
      //os_printf("IP sent to LCD = %s %d\r\n", busdata, os_strlen(busdata));
      os_free(busdata);

      system_os_post(USER_TASK_PRIO_0, tcp_SET_LISTENER, (os_param_t) NULL);
      break;
    default:
      ;
  }
}

