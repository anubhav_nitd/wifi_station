#include "bus.h"
#include "driver/i2c.h"
#include <c_types.h>
#include <osapi.h>

static uint8 i;

void ICACHE_FLASH_ATTR bus_put_data(uint8 address, uint8 *data, uint8 data_len) {
  //os_printf("Address = %02x.\r\n", address);
  i2c_start();
  i2c_writeByte(address << 1 | 0);
  i2c_check_ack();

  for(i = 0; i < data_len; i++) {
    i2c_writeByte(data[i]);
    //os_printf("Data = %02x.\r\n", data[i]);
    i2c_check_ack();
  }
  i2c_stop();
}
