#include "tasks.h"
#include <ets_sys.h>
#include "tcp.h"
#include "http.h"
#include <osapi.h>

os_event_t task_Q_P0[task_QLEN_P0];

void ICACHE_FLASH_ATTR task_priority_zero_handler(os_event_t *event) {
  // if the task was posted to wrong queue
  if(event->sig < 0 || event->sig > 99) {
    //os_printf("TASK ERROR : Wrong task posted in Priority 0 queue. Task number = ");
    //os_printf("%d.\n", event->sig);
    return;
  }

  switch(event->sig) {
    case tcp_SET_LISTENER:
      tcp_set_listener();
      break;

    case http_SERVE_GET_REQUESTS:
      http_serve_GET_requests((struct tcp_req_params *) event->par);
      break;

    case tcp_SCHEDULE_DATA_TX:
      tcp_schedule_data_tx((struct tcp_txdata_node *) event->par);
      break;

    case dvc_TURN_OFF:
      dvc_turn_off((uint8 *) event->par);

    case dvc_TURN_ON:
      dvc_turn_on((uint8 *) event->par);
    
    case dvc_TOGGLE:
      dvc_toggle((uint8 *) event->par);

    default:
      return;
  }
}

