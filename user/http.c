#include "http.h"
#include <osapi.h>
#include <mem.h>
#include "html_docs/home.h"
#include "tasks.h"
#include <c_types.h>
#include <user_interface.h>
#include <espconn.h>
#include "user_config.h"
#include "dvc.h"

//char *http_ver      = "HTTP/1.0 ";
char http_ok[]       = "HTTP/1.0 200 OK\r\n\r\n";
char http_notfound[] = "HTTP/1.0 404 Not Found\r\n\r\n";
//char http_moved[]    = "HTTP/1.0 302 Moved\r\n\r\n";
char http_moved[]    = "HTTP/1.0 302 Moved\r\n";

char  location[50]   = "Location: ";
uint8 loc_fixed_sz   = 10;


void ICACHE_FLASH_ATTR http_serve_GET_requests(struct tcp_req_params *params) {
  if(params->prequest[os_strlen("GET /")] == ' ') {
    struct tcp_txdata_node *header = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    header->client                 = params->client;
    header->data                   = http_moved;
    header->data_len               = os_strlen(http_moved);

    struct tcp_txdata_node *more_of_header = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    //os_sprintf(location + loc_fixed_sz, IPSTR "\r\n", IP2STR(&params->client->proto.tcp->local_ip));
    //os_sprintf(location + loc_fixed_sz, IPSTR "\r\n", IP2STR(& user_conf_current_conn_ip.ip.addr));
    os_sprintf(location + loc_fixed_sz, "home.html\r\n");
    more_of_header->data                   = location;
    more_of_header->data_len               = strlen(location);
    more_of_header->next                   = NULL;

    header->next = more_of_header;

    struct tcp_txdata_node *brak = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    brak->data                   = NULL; // if encountered, disconnect tcp session

    more_of_header->next = brak;

    system_os_post(USER_TASK_PRIO_0, tcp_SCHEDULE_DATA_TX, (os_param_t) header);
  }

  else if(os_strncmp((params->prequest+os_strlen("GET /")), "home.html" , 9) == 0) {
    struct tcp_txdata_node *header = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    header->client                 = params->client;
    header->data                   = http_ok;
    header->data_len               = os_strlen(http_ok);

    struct tcp_txdata_node *page = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    page->data                   = home_html;
    page->data_len               = home_html_len;
    page->next                   = NULL;

    header->next = page;

    struct tcp_txdata_node *brak = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    brak->data                   = NULL; // if encountered, disconnect tcp session

    page->next = brak;

    system_os_post(USER_TASK_PRIO_0, tcp_SCHEDULE_DATA_TX, (os_param_t) header);
  }

  else if(os_strncmp((params->prequest+os_strlen("GET /")), "state?dvc_idx=" , 14) == 0) {
    uint8 *dvc_idx = (uint8 *) os_malloc(sizeof(uint8));
    *dvc_idx = 0;
    uint8 i;
    for(i = os_strlen("GET /state?dvc_idx="); isdigit(params->prequest[i]); i++) {
      *dvc_idx = (*dvc_idx) * 10 + params->prequest[i] - '0';
    }

    system_os_post(USER_TASK_PRIO_0, dvc_TOGGLE, (os_param_t) dvc_idx);

    struct tcp_txdata_node *header = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    header->client                 = params->client;
    header->data                   = http_moved;
    header->data_len               = os_strlen(http_moved);

    struct tcp_txdata_node *more_of_header = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    os_sprintf(location + loc_fixed_sz, "home.html\r\n");
    more_of_header->data                   = location;
    more_of_header->data_len               = strlen(location);
    more_of_header->next                   = NULL;

    header->next = more_of_header;

    struct tcp_txdata_node *brak = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    brak->data                   = NULL; // if encountered, disconnect tcp session

    more_of_header->next = brak;

    system_os_post(USER_TASK_PRIO_0, tcp_SCHEDULE_DATA_TX, (os_param_t) header);
  }

  else if(os_strncmp((params->prequest+os_strlen("GET /")), "dvc_data" , 8) == 0) {
    
    struct tcp_txdata_node *header = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    header->client                 = params->client;
    header->data                   = http_ok;
    header->data_len               = os_strlen(http_ok);

    struct tcp_txdata_node *dvc_data_json = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    dvc_data_json->data                   = dvc_get_data_json();
    dvc_data_json->data_len               = strlen(dvc_data_json->data);
    dvc_data_json->next                   = NULL;

    header->next = dvc_data_json;

    struct tcp_txdata_node *brak = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    brak->data                   = NULL; // if encountered, disconnect tcp session

    dvc_data_json->next = brak;

    system_os_post(USER_TASK_PRIO_0, tcp_SCHEDULE_DATA_TX, (os_param_t) header);
  }

  else /*if(os_strncmp((params->prequest+os_strlen("GET /")), "favicon.ico" , 11) == 0)*/ {
    struct tcp_txdata_node *brak = (struct tcp_txdata_node *) os_malloc(sizeof(struct tcp_txdata_node));
    brak->client                 = params->client;
    brak->data                   = NULL; // if encountered, disconnect tcp session


    system_os_post(USER_TASK_PRIO_0, tcp_SCHEDULE_DATA_TX, (os_param_t) brak);
  }

  os_free(params);
}
