#include "dvc.h"
#include <c_types.h>
#include <mem.h>
#include <osapi.h>

/*ICACHE_FLASH_ATTR*/ uint8 dvc_count = 4;

/*ICACHE_FLASH_ATTR*/ char *dvc_name[] = {
  "Drawing Room CFL",               // #0
  "Drawing Room Fan",               // #1
  "Microwave Oven",                 // #2
  "Refrigerator"                    // #3
};

/*ICACHE_FLASH_ATTR*/ uint32 dvc_addr[] = {
  // First byte (MSB) is the address of the slave on the bus.
  // Second byte is the port number at which the device is connected to the slave.
  //   This port number can be interpreted differently by different intelligent slaves.
  //   Eg for PIC16F877A, port address 0 -> PORTB, port address 2 -> PORTD.
  // Third byte has only one bit set, and evaluates to the bit mask of the port pin to which the device is conneted.
  // Fourth byte (LSB) represents the pin behavior for each pin of the parent port.
  //   If bit n of 4th byte is 1, the pin has an active high device.
  //   Similarly, if that bit is 0, the device connected is active low.

  // IMP: Slave 01, port 00 (0x0100xxxx) is reserved for LCD. Avoid assigning it to any device.

  0x01020100,                       // #0
  0x01020200,                       // #1
  0x01020400,                       // #2
  0x01020800                        // #3
};

/*ICACHE_FLASH_ATTR*/ enum dvc_STATE dvc_state[] = {
  OFF,                              // #0
  OFF,                              // #1
  OFF,                              // #2
  OFF                               // #3
};

char *dvc_data_json;

static uint8 dvc_data_mem_incr_val = 100;
static uint8 dvc_data_mem_incr_cnt = 0;
void dvc_init_data_json() {
  dvc_data_json = (char *) os_malloc(sizeof(char) * dvc_data_mem_incr_val);
  dvc_data_mem_incr_cnt++;
  os_sprintf(dvc_data_json, "{\"dvc_count\":");
  os_sprintf(dvc_data_json + os_strlen(dvc_data_json), "%u,", dvc_count);
  os_sprintf(dvc_data_json + os_strlen(dvc_data_json), "\"dvc_list\":[");

  uint8 i;
  for(i = 0; i < dvc_count; i++) {
    if((dvc_data_mem_incr_val * dvc_data_mem_incr_cnt) - os_strlen(dvc_data_json) < dvc_data_mem_incr_val) {
      dvc_data_mem_incr_cnt++;
      dvc_data_json = (char *) os_realloc(dvc_data_json, dvc_data_mem_incr_val * dvc_data_mem_incr_cnt);
    }

    os_sprintf(dvc_data_json + strlen(dvc_data_json), "{\"name\":");
    os_sprintf(dvc_data_json + strlen(dvc_data_json), "\"%s\",", dvc_name[i]);
    os_sprintf(dvc_data_json + strlen(dvc_data_json), "\"address\":");
    os_sprintf(dvc_data_json + strlen(dvc_data_json), "%4x,", dvc_addr[i]);
    os_sprintf(dvc_data_json + strlen(dvc_data_json), "\"status\":");
    if(dvc_state[i] == OFF) {
      os_sprintf(dvc_data_json + strlen(dvc_data_json), "\"%s\"", "OFF");
    }
    else {
      os_sprintf(dvc_data_json + strlen(dvc_data_json), "\"%s\"", "ON");
    }
    os_sprintf(dvc_data_json + strlen(dvc_data_json), "},");
  }
  os_sprintf(dvc_data_json + strlen(dvc_data_json) - 1, "]}"); // -1 to remove comma after last array element
}

char *dvc_get_data_json() {
  if(dvc_data_json != NULL) {
    os_free(dvc_data_json);
    dvc_data_mem_incr_cnt = 0;
    dvc_init_data_json();
    return dvc_data_json;
  }
  else return NULL;
}
void ICACHE_FLASH_ATTR dvc_turn_off(uint8 *dvc_idx) {
  if(*dvc_idx > dvc_count) {
    os_free(dvc_idx);
    return;
  }
  uint8  busdata_len   = 3;
  uint8 *busdata       = (uint8 *) os_malloc(sizeof(uint8) * busdata_len);
  uint8  bus_dest_addr = (uint8) (dvc_addr[*dvc_idx] >> 24) & 0xff;
  busdata[0]           = (uint8) (dvc_addr[*dvc_idx] >> 16) & 0xff;     // port
  busdata[1]           = (uint8) (dvc_addr[*dvc_idx] >> 8) & 0xff;      // new bit mask for port
  //busdata[2]           = 0x00;                                          // 0 = port bits clear mask
  busdata[2]           = (((uint8) (dvc_addr[*dvc_idx]) & 0xff) & busdata[1]) == 0 ? 0x01 : 0x00;

  bus_put_data(bus_dest_addr, busdata, busdata_len);
  os_free(busdata);
  dvc_state[*dvc_idx] = OFF;
  os_free(dvc_idx);
}

void ICACHE_FLASH_ATTR dvc_turn_on(uint8 *dvc_idx) {
  if(*dvc_idx > dvc_count) {
    os_free(dvc_idx);
    return;
  }
  uint8  busdata_len   = 3;
  uint8 *busdata       = (uint8 *) os_malloc(sizeof(uint8) * busdata_len);
  uint8  bus_dest_addr = (uint8) (dvc_addr[*dvc_idx] >> 24) & 0xff;
  busdata[0]           = (uint8) (dvc_addr[*dvc_idx] >> 16) & 0xff;     // port
  busdata[1]           = (uint8) (dvc_addr[*dvc_idx] >> 8) & 0xff;     // new bit mask for port
  //busdata[2]           = 0x01;                                          // 1 = port bits set mask
  busdata[2]           = (((uint8) (dvc_addr[*dvc_idx]) & 0xff) & busdata[1]) == 0 ? 0x00 : 0x01;

  bus_put_data(bus_dest_addr, busdata, busdata_len);
  os_free(busdata);
  dvc_state[*dvc_idx] = ON;
  os_free(dvc_idx);
}

void ICACHE_FLASH_ATTR dvc_toggle(uint8 *dvc_idx) {
  // This function could have called dvc_turn_on or dvc_turn_off based on the device state,
  // but we intend to free the dynamically allocated parameters passed by system calls in
  // the called function itself. So we do everything explicitly here to avoid inconsistency.

  if(*dvc_idx > dvc_count) {
    os_free(dvc_idx);
    return;
  }
  uint8  busdata_len   = 3;
  uint8 *busdata       = (uint8 *) os_malloc(sizeof(uint8) * busdata_len);
  uint8  bus_dest_addr = (uint8) (dvc_addr[*dvc_idx] >> 24) & 0xff;
  busdata[0]           = (uint8) (dvc_addr[*dvc_idx] >> 16) & 0xff;     // port
  busdata[1]           = (uint8) (dvc_addr[*dvc_idx] >> 8) & 0xff;      // new bit mask for port
  busdata[2]           = (dvc_state[*dvc_idx] == OFF) ?
                              ((((uint8) (dvc_addr[*dvc_idx]) & 0xff) & busdata[1]) == 0 ? 0x00 : 0x01)
                            : ((((uint8) (dvc_addr[*dvc_idx]) & 0xff) & busdata[1]) == 0 ? 0x01 : 0x00);

  bus_put_data(bus_dest_addr, busdata, busdata_len);
  os_free(busdata);
  dvc_state[*dvc_idx] = (dvc_state[*dvc_idx] == OFF ? ON : OFF);
  os_free(dvc_idx);
}


/*===================================== LCD ===========================================*/

static uint8 lcd_slave_addr = 0x01;
static uint8 lcd_port_addr  = 0x00;                    // PORTB on PIC16F877A having I2C slave address 0x01
static uint8 lcd_delay_us   = 5;
static uint8 lcd_cursor_current_address, i;

static union {
  struct {
    uint8 lcd_nc        : 1;                     // port pin 0 is not connected to any LCD pin
    uint8 lcd_rs        : 1;                     // RS pin is connected to port pin 1
    uint8 lcd_rw        : 1;                     // RW pin is connected to port pin 2
    uint8 lcd_en        : 1;                     // EN pin is connected to port pin 3
    uint8 lcd_d4        : 1;                     // D4 pin is connected to port pin 4
    uint8 lcd_d5        : 1;                     // D5 pin is connected to port pin 5
    uint8 lcd_d6        : 1;                     // D6 pin is connected to port pin 6
    uint8 lcd_d7        : 1;                     // D7 pin is connected to port pin 7
  } pins;
  uint8 val;
} lcd_port_val;

uint8 ICACHE_FLASH_ATTR dvc_lcd_init() {
  lcd_port_val.val = 0;
  lcd_port_val.pins.lcd_d5 = 1;
  lcd_port_val.pins.lcd_d4 = 1;

  // First byte is port address
  // Second byte is new bit mask
  // Third byte specifies action - set mask(1), clear mask(0), overwrite mask(2)
  uint8 *busdata = (uint8 *) os_malloc(sizeof(uint8) * 3);
  busdata[0] = lcd_port_addr;

  //busdata[1] = lcd_port_val.val;
  busdata[2] = 0x02;                                         // overwrite mask

  // LCD init sequence
  for(i = 0; i < 6; i++) {
    lcd_port_val.pins.lcd_en ^= 1;
    busdata[1] = lcd_port_val.val;
    bus_put_data(lcd_slave_addr, busdata, 3);
    os_delay_us(2000);
  }

  // Set LCD interface mode to 4-bit
  lcd_port_val.val         = 0;
  lcd_port_val.pins.lcd_d5 = 1;
  lcd_port_val.pins.lcd_en = 1;
  busdata[1] = lcd_port_val.val;
  bus_put_data(lcd_slave_addr, busdata, 3);
  os_delay_us(lcd_delay_us);
  lcd_port_val.pins.lcd_en = 0;
  busdata[1] = lcd_port_val.val;
  bus_put_data(lcd_slave_addr, busdata, 3);
  os_delay_us(lcd_delay_us);

  os_free(busdata);

  dvc_lcd_send_command(0x2c);                         // 4-bit, 2 rows, 5x10 font
  //os_delay_us(lcd_delay_us);
  dvc_lcd_send_command(0x0e);                         // display on, cursor on, blink off
  //os_delay_us(lcd_delay_us);
  dvc_lcd_send_command(0x06);                         // cursor direction = increment, display shift disabled
  //os_delay_us(lcd_delay_us);

  dvc_lcd_clear();
  return 0;
}

void ICACHE_FLASH_ATTR dvc_lcd_send_command(const uint8 cmd) {
  // First byte is port address
  // Second byte is new bit mask
  // Third byte specifies action - set mask(1), clear mask(0), overwrite mask(2)
  uint8 *busdata = (uint8 *) os_malloc(sizeof(uint8) * 3);
  busdata[0] = lcd_port_addr;
  busdata[2] = 0x02;                                         // overwrite mask

  // Send MSNibble
  lcd_port_val.val         = 0;
  lcd_port_val.pins.lcd_rs = 0;
  lcd_port_val.pins.lcd_en = 0;
  lcd_port_val.pins.lcd_rw = 0;
  lcd_port_val.pins.lcd_d7 = (cmd & 0x80) >> 7;
  lcd_port_val.pins.lcd_d6 = (cmd & 0x40) >> 6;
  lcd_port_val.pins.lcd_d5 = (cmd & 0x20) >> 5;
  lcd_port_val.pins.lcd_d4 = (cmd & 0x10) >> 4;
  
  for(i = 0; i < 2; i++) {
    lcd_port_val.pins.lcd_en ^= 1;
    busdata[1] = lcd_port_val.val;
    bus_put_data(lcd_slave_addr, busdata, 3);
    os_delay_us(lcd_delay_us);
  }

  // Send LSNibble
  lcd_port_val.val         = 0;
  lcd_port_val.pins.lcd_rs = 0;
  lcd_port_val.pins.lcd_en = 0;
  lcd_port_val.pins.lcd_rw = 0;
  lcd_port_val.pins.lcd_d7 = (cmd & 0x08) >> 3;
  lcd_port_val.pins.lcd_d6 = (cmd & 0x04) >> 2;
  lcd_port_val.pins.lcd_d5 = (cmd & 0x02) >> 1;
  lcd_port_val.pins.lcd_d4 = (cmd & 0x01) >> 0;
  
  for(i = 0; i < 2; i++) {
    lcd_port_val.pins.lcd_en ^= 1;
    busdata[1] = lcd_port_val.val;
    bus_put_data(lcd_slave_addr, busdata, 3);
    os_delay_us(lcd_delay_us);
  }

  os_free(busdata);
  
  // a command was given to set DDRAM to a specified address
  // TODO: What will happen in case of an invalid address? (col > 40)
  if(cmd & 0x80) lcd_cursor_current_address = cmd & 0x7f;

  // clear display, or set cursor to home
  else if(cmd == 0x01 || cmd == 0x02) {
    lcd_cursor_current_address = 0;
    os_delay_us(2000);
  }

  // cursor shift
  // TODO: What will happen in case of an address overflow? (col > 40)
  else if((cmd & 0xf8) == 0x10) {
    if(cmd & 0x04) lcd_cursor_current_address++;
    else if(lcd_cursor_current_address) lcd_cursor_current_address--;
  }
}

void ICACHE_FLASH_ATTR dvc_lcd_send_data(const uint8 *data, const uint8 data_len) {
  //os_printf("Data = %s\r\n", data);
  //os_printf("Data length = %d\r\n", data_len);

  // First byte is port address
  // Second byte is new bit mask
  // Third byte specifies action - set mask(1), clear mask(0), overwrite mask(2)
  uint8 *busdata = (uint8 *) os_malloc(sizeof(uint8) * 3);
  busdata[0] = lcd_port_addr;
  busdata[2] = 0x02;                                         // overwrite mask

  int j;
  for(j = 0; j < data_len; j++) {
    // Send MSNibble
    lcd_port_val.val         = 0;
    lcd_port_val.pins.lcd_rs = 1;
    lcd_port_val.pins.lcd_en = 0;
    lcd_port_val.pins.lcd_rw = 0;
    lcd_port_val.pins.lcd_d7 = (data[j] & 0x80) >> 7;
    lcd_port_val.pins.lcd_d6 = (data[j] & 0x40) >> 6;
    lcd_port_val.pins.lcd_d5 = (data[j] & 0x20) >> 5;
    lcd_port_val.pins.lcd_d4 = (data[j] & 0x10) >> 4;
    
    for(i = 0; i < 2; i++) {
      lcd_port_val.pins.lcd_en ^= 1;
      busdata[1] = lcd_port_val.val;
      bus_put_data(lcd_slave_addr, busdata, 3);
      os_delay_us(lcd_delay_us);
    }

    // Send LSNibble
    lcd_port_val.val         = 0;
    lcd_port_val.pins.lcd_rs = 1;
    lcd_port_val.pins.lcd_en = 0;
    lcd_port_val.pins.lcd_rw = 0;
    lcd_port_val.pins.lcd_d7 = (data[j] & 0x08) >> 3;
    lcd_port_val.pins.lcd_d6 = (data[j] & 0x04) >> 2;
    lcd_port_val.pins.lcd_d5 = (data[j] & 0x02) >> 1;
    lcd_port_val.pins.lcd_d4 = (data[j] & 0x01) >> 0;
    
    for(i = 0; i < 2; i++) {
      lcd_port_val.pins.lcd_en ^= 1;
      busdata[1] = lcd_port_val.val;
      bus_put_data(lcd_slave_addr, busdata, 3);
      os_delay_us(lcd_delay_us);
    }

    lcd_cursor_current_address++;
  }
  os_free(busdata);  
}

void dvc_lcd_clear() {
  dvc_lcd_send_command(0x01);
}

void dvc_lcd_gotoRC(const uint8 row, const uint8 col) {
  lcd_cursor_current_address = ((row > 1 ? 1 : row) << 6) | (col > 15 ? 0x0f : col);
  dvc_lcd_send_command(lcd_cursor_current_address | 0x80);
}
