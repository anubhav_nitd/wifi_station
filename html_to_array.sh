#! /bin/sh

input_file_name=$1
output_dir=$2

var_name=`echo $input_file_name | sed 's/\.html/_html/'`
var_len=$var_name'_len'

var_name='unsigned char '$var_name'[]'
var_len='unsigned int '$var_len

header_name=`echo $input_file_name | sed 's/\.html/\.h/'`
header_file=$output_dir'/'$header_name

# clear header file
echo '' > $header_file

# feed data into header file
# echo #ifndef `echo $header_name | tr [a-z] [A-Z] | sed 's/\./_/'`
echo '#ifndef '`echo $header_name | awk '{print toupper($0)}' | sed 's/\./_/'` > $header_file
echo '#define '`echo $header_name | awk '{print toupper($0)}' | sed 's/\./_/'` >> $header_file
echo '' >> $header_file
echo '' >> $header_file
echo 'extern '$var_name';' >> $header_file
echo 'extern '$var_len';' >> $header_file
echo '' >> $header_file
echo '#endif // #ifndef '`echo $header_name | awk '{print toupper($0)}' | sed 's/\./_/'` >> $header_file

source_file=`echo $header_file | sed 's/\.h/\.c/'`

echo '#include "html_docs/'$header_name'"' > $source_file

# This is not needed, as we are not putting the variables in flash
#echo '#include <c_types.h>' >> $source_file
echo '' >> $source_file
xxd -i $input_file_name >> $source_file

# DO NOT USE THESE, OTHERWISE ESP8266 THROWS 'FATAL EXCEPTION 3 LoadStoreErrorCause'
#sed -i 's/ char / char ICACHE_FLASH_ATTR /' $source_file
#sed -i 's/ int / int ICACHE_FLASH_ATTR /' $source_file
