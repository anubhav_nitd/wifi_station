#ifndef BUS_H
#define BUS_H

#include <c_types.h>

void bus_put_data(uint8 address, uint8 *data, uint8 data_len);

#endif // #ifndef BUS_H
