#ifndef TASKS_H
#define TASKS_H


#include <os_type.h>
#include <c_types.h>

/***********************************************
 * Priority 0 tasks' definitions and functions *
 ***********************************************/

enum {
  // All tasks with priority 0 will be enumerated here.
  // These enums will be used as signal fields.
  // Make sure these enums resolve to numbers between 0 and 99.
  tcp_SET_LISTENER = 0,
  http_SERVE_GET_REQUESTS,
  tcp_SCHEDULE_DATA_TX,
  dvc_TURN_OFF,
  dvc_TURN_ON,
  dvc_TOGGLE
};

#define task_QLEN_P0 20
extern os_event_t task_Q_P0[task_QLEN_P0];

void ICACHE_FLASH_ATTR task_priority_zero_handler(os_event_t *event);


#endif // #ifndef TASKS_H
