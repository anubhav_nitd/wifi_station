#ifndef HTTP_H
#define HTTP_H

#include "tcp.h"


void ICACHE_FLASH_ATTR http_serve_GET_requests(struct tcp_req_params *params);

#endif // #ifndef HTTP_H
