#ifndef TCP_H
#define TCP_H

#include <c_types.h>

// TODO: Find out if we can use espconn_state field of esp_tcp instead of this.
// As of now, different progress states for different remote ip/port pairs is not being maintained.
extern bool tcp_tx_in_progress;

struct tcp_req_params {
  struct espconn *client;
  char           *prequest;
};

struct tcp_txdata_node {
  struct espconn         *client;
  char                   *data;
  unsigned short          data_len;
  struct tcp_txdata_node *next;
};

void ICACHE_FLASH_ATTR tcp_set_listener();
void ICACHE_FLASH_ATTR tcp_schedule_data_tx(struct tcp_txdata_node *list);

#endif // #ifndef TCP_H
