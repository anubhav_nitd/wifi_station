#ifndef DVC_H
#define DVC_H

#include <c_types.h>

enum dvc_STATE {
  OFF,
  ON
};

extern char           *dvc_name[];
extern uint32          dvc_addr[];
extern enum dvc_STATE  dvc_state[];
extern uint8           dvc_count;
extern char           *dvc_data_json;

void dvc_turn_off(uint8 *dvc_idx);
void dvc_turn_on(uint8 *dvc_idx);
void dvc_toggle(uint8 *dvc_idx);
char *dvc_get_data_json();
void dvc_init_data_json();



/* LCD */

// All control pins of LCD should be connected to only one port in the slave,
//and LCD should be operated in 4-bit mode.

uint8 dvc_lcd_init();
void  dvc_lcd_send_command(const uint8 cmd);
void  dvc_lcd_send_data(const uint8 *data, const uint8 data_len);
void  dvc_lcd_clear();
void  dvc_lcd_gotoRC(const uint8 row, const uint8 col);

#endif // #ifndef DVC_H
